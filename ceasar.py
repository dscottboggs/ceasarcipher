import string
original_alphabet=string.ascii_lowercase
def convert_alphabet(shift: int):
    while shift>len(original_alphabet):
        shift-=len(original_alphabet)
    for letter_number in range(len(original_alphabet)):
        if letter_number+shift >= len(original_alphabet):
            letter_number-=len(original_alphabet)
        yield original_alphabet[letter_number+shift]

def convert_alphabet(shift: int):
    return original_alphabet[:0-shift] + original_alphabet[0-shift:]

_, msg, shift = map(input, range(3))
alphabet = [ letter for letter in convert_alphabet(int(shift))]
msgout = ''
for letter in msg:
    try:
        idx = original_alphabet.index(letter)
    except ValueError:
        print(letter)
        msgout+=letter
    if idx >= len(original_alphabet):
        idx-=len(original_alphabet)
    print(alphabet[idx], original_alphabet[idx])
    msgout += alphabet[idx]
print(msgout)
